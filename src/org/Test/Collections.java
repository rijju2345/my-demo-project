package org.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Collections {
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		//Loading our driver
		Class.forName("oracle.jdbc.driver.OracleDriver");
		
		//2. Set up Connection
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "hr", "Jawaharnis@99");
		//url---> jdbc:oracle:thin:@localhost:1521:xe (Default)
		//oracle: Database name
		//thin: Driver.
		//@localhost: IP address where database is stored
		//1521: port number
		//xe: service number
		
		//3.write a query
		String query = "Select * from employees";
		
		//4. prepare statement
		PreparedStatement ps = connection.prepareStatement(query);
		
		//5.Execute the query
		ResultSet executeQuery = ps.executeQuery();
		
		//6. iterate the result
		
		while(executeQuery.next()) {
			
			int int1 = executeQuery.getInt("employee_ID");
			System.out.println("employee id "+ int1);
			
			String string = executeQuery.getString("first_name");
			System.out.println("employee first name" +string);
		}
		
		//close the connection
		connection.close();
		
	}
	

}
