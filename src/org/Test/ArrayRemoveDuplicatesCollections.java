package org.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class ArrayRemoveDuplicatesCollections {
	public static void main(String[] args) {
		Integer[]  arr = {10,10,20,50,60,80,60,50};
		System.out.println("Length: " +arr.length);
		System.out.println("Array Elements: " + Arrays.toString(arr));
		Set<Integer> setArr = new HashSet<Integer>(Arrays.asList(arr));
		
		System.out.println("after removal length:" +setArr.size());
		System.out.println("Dupliates" +setArr);

}
}