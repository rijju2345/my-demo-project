package org.Test;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class DataTable {
	
	public static void main(String[] args) {
		
		//1D without header
		
		List<String> li = new ArrayList<String>();
		li.add("sabasiva");
		li.add("saba@gmail.com");
		li.add("saba@1");
		li.add("chennai");
		
		String string = li.get(1);
		System.out.println(string);
		
		//1D with header
		
		Map<String, String> mp = new LinkedHashMap<String, String>();
		mp.put("userName", "SabaSiva");
		mp.put("emailId", "Saba@gmail.com");
		mp.put("password", "SabaSiva");
		mp.put("location", "SabaSiva");
		
		String string2 = mp.get("location");
		System.out.println(string2);
		
		//2D Without header
		List<ArrayList<String>> lis = new ArrayList<>();
		//we need to add value to column
		ArrayList<String> ai1 = new ArrayList<String>();
		ai1.add("vel");
		ai1.add("riswan@gmail.com");
		ai1.add("riswan@4");
		ai1.add("chennai");
		
		
		ArrayList<String> ai11 = new ArrayList<String>();
		ai11.add("mohamed");
		ai11.add("mohamed@gmail.com");
		ai11.add("mohamed@4");
		ai11.add("madurai");
		

		ArrayList<String> ai111 = new ArrayList<String>();
		ai111.add("rijju");
		ai111.add("rijju@gmail.com");
		ai111.add("rijju@4");
		ai111.add("madurai");
		
		lis.add(ai1);
		lis.add(ai11);
		lis.add(ai111);
		
		ArrayList<String> arrayList = lis.get(1);
		String string3 = arrayList.get(1);
		System.out.println(string3);
		
		//2D with header
		List<LinkedHashMap<String, String>> list = new ArrayList<LinkedHashMap<String, String>>();
		LinkedHashMap<String, String> mp1 = new LinkedHashMap<String, String>();
		mp1.put("username", "Mohamed");
		mp1.put("email", "Mohamed@gmail.com");
		mp1.put("password", "Mohamed");
		mp1.put("locatin", "chennai");
		
		LinkedHashMap<String, String> mp11 = new LinkedHashMap<String, String>();
		mp11.put("username", "Mohamed");
		mp11.put("email", "Mohamed@gmail.com");
		mp11.put("password", "Mohamed");
		mp11.put("locatin", "chennai");
		list.add(mp1);
		list.add(mp11);
		
		LinkedHashMap<String, String> linkedHashMap = list.get(0);
		String string4 = linkedHashMap.get("username");
		System.out.println(string4);
				
		
		
		
		
	}

}
